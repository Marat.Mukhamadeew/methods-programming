package by.bsu.finalvar;

public class Card {
    // Инициализированная константа экземпляра
    public final int ID = (int) (Math.random() * 10_000_000);
    // Неициализированная константа
    public final long BANK_ID;    // Инициализация по умолчанию не производится
//    { BANK_ID = 11111111L; }      // Только один раз

    public Card(long BANK_ID) {
        // Инициализация в конструкторе
        this.BANK_ID = BANK_ID; // Только один раз
    }

    public final boolean checkRights(final int NUMBER) {
        final int CODE = 72173394;    // антишаблон: "Волшебгое Число"
        return CODE == NUMBER + ID;
    }
}
