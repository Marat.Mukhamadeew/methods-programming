package by.bsu.format;

import java.util.Formatter;

public class FormatterDemoFlags {
    public static void main(String[] args) {
        Formatter f = new Formatter();
        // Выравнивание вправо
        f.format("|%10.2f", 123.123);
        System.out.println(f);

        // Выравнивание влево
        // применение флага '-'
        f = new Formatter();
        f.format("|%-10.2f", 123.123);
        System.out.println(f);

        f = new Formatter();
        f.format("% (d", -100);
        // Применение флага '' и '('
        System.out.println(f);

        f = new Formatter();
        f.format("%,.2f", 123456789.34);
        // Применение флага ','
        System.out.println(f);

        f = new Formatter();
        f.format("%.4f", 1111.11111111);
        // Задание точности представления чисел
        System.out.println(f);

        f = new Formatter();
        f.format("%.16s", "Now I know class java.util.Formatter");
        // Задание точности представления для строк
        System.out.println(f);
    }
}
