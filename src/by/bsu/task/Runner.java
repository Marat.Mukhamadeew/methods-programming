package by.bsu.task;

import by.bsu.task.action.Multiplicator;
import by.bsu.task.creator.MatrixCreator;
import by.bsu.task.entity.Matrix;
import by.bsu.task.exception.MatrixException;

public class Runner {
    public static void main(String[] args) {
        try {
            Matrix p = new Matrix(2, 3);
            System.out.println(p.getVerticalSize());
            System.out.println(p.getHorizontalSize() + " - p.getHorizontal");
            MatrixCreator.fillRandomized(p, 2, 8);
            System.out.println("Matrix first is: " + p);
            Matrix q = new Matrix(3,4);
            System.out.println(q.getVerticalSize());
            System.out.println(q.getHorizontalSize() + " - q.getHorizontal");
            MatrixCreator.fillRandomized(q,2,7);
            System.out.println("Matrix second is: " + q);
            System.out.println("Matrices product is " + Multiplicator.multiply(p,q));
        } catch (MatrixException e) {
            System.err.println("Error of creating matrix " + e);
        }
    }
}
