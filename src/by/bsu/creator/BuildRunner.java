package by.bsu.creator;

import by.bsu.point.Point2D;
import by.bsu.point.Point3D;

public class BuildRunner {
    public static void main(String[] args) {
        Point2DCreator br = new Point3DCreator();
//        Point3D p = br.createPoint();
        Point2D p = br.createPoint();   // "раннее связывание"
        System.out.println(br.createPoint().toString());
        System.out.println(p.getClass());
    }
}
