package by.bsu.collect;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AlgoritmDemo {
    public static void main(String[] args) {
        Comparator<Integer> comp = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        };
        ArrayList<Integer> list = new ArrayList<>();
        Collections.addAll(list, 1, 2, 3, 4, 5);
        Collections.shuffle(list);
        print(list);
        Collections.sort(list, comp);
        print(list);
        Collections.reverse(list);
        print(list);
        Collections.rotate(list, 3);
        System.out.println("min: " + Collections.min(list, comp));
        System.out.println("max: " + Collections.max(list, comp));

        List<Integer> signl = Collections.singletonList(71);
        print(signl);
//        signl.add(21);  // Ошибка времени выполнения
    }

    private static void print(List<Integer> c) {
        for (Integer i : c) {
            System.out.print(i + " ");
        }
        System.out.println();
    }
}
