package by.bsu.collect;

import java.util.ArrayList;

public class DemoGeneric {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
//        ArrayList<int> b = new ArrayList<int>();   // Ошибка компиляции
        list.add("Java");   // Компилятор "знает" допустимый тип передаваемого значения
        list.add("JavaFX 2");
        String res = list.get(0); // Компилятор "знает" допустимый тип возвращаемого значения
//        list.add(new StringBuilder("C#")); // Компилятор не позволяет добавить "посторонний" тип
        System.out.println(list);
    }
}
