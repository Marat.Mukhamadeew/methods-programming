package by.bsu.fund.entity;

import by.bsu.fund.exceptions.CoinLogicException;
import by.bsu.fund.exceptions.CoinTechnicalException;

public class Coin {
    private double diameter;
    private double weight;

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) throws CoinLogicException {
        if (diameter <= 0) {
            throw new CoinLogicException("Diameter is incorrect");
        }
        this.diameter = diameter;
    }

    public Coin(double diameter) throws CoinLogicException {
        super();
        setDiameter(diameter);
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public void doAction(String value) throws CoinTechnicalException {
        try {
            Coin ob = new Coin(55.5);
            double d = Double.parseDouble(value);
            ob.setDiameter(d);
        } catch (NumberFormatException e) {
            throw new CoinTechnicalException("incorrect symbol in string", e);
        } catch (CoinLogicException e) {
            System.err.println(e.getCause());
        }
    }
}
