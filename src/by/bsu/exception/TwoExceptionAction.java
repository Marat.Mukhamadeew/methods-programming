package by.bsu.exception;

public class TwoExceptionAction {
    public void doAction() {
        try {
            int a = (int) (Math.random() * 2);
            System.out.println("a = " + a);
            int[] c = { 1 / a };    // Опасное место №1
            c[a] = 71;  // Опасное место №2
        } catch (ArithmeticException e) {
            System.err.println("Деление на ноль " + e);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.err.println("out of bound: " + e);
        }   // Окончание try-catch блока
        System.out.println("After try-catch");
    }
}
