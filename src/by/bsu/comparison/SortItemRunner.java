package by.bsu.comparison;

import by.bsu.collection.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class SortItemRunner {
    public static void main(String[] args) {
        ArrayList<Item> p = new ArrayList<>() {
            {
                add(new Item(52201,9.75f,"T-Shirt"));
                add(new Item(52127,13.99f,"Dress"));
                add(new Item(47063,45.95f,"Jeans"));
                add(new Item(90428,60.9f,"Gloves"));
                add(new Item(53295,31f,"Shirt"));
                add(new Item(63220,14.9f,"Tie"));
            }
        };
        // Создание компаратора
        Comparator<Item> comp = new Item.PriceComparator(); //{
//            // Сравнение для сортировки по убыванию цены товара
//            @Override
//            public int compare(Item o1, Item o2) {
//                return Double.compare(o2.getPrice(), o1.getPrice());
//            }
////            public boolean equals(Object ob) { // Реализация }
//        };
        // Сортировка списка объектов
        Collections.sort(p,comp);
        System.out.println(p);
    }
}
