package by.bsu.tranformation;

public class StringToInt {
    public static void main(String[] args) {
        String arg = "71";  // 071 или 0x71 или 0b1000111
        try {
            int value1 = Integer.parseInt(arg); // возвращает int
            int value2 = Integer.valueOf(arg);  // возвращает Integer
            int value3 = Integer.decode(arg);   // Возвращает Integer
//            int value4 = new Integer(arg);      // Создает Integer, для преобразования применяется
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат числа " + e);
        }
        int g = defineLevel("moderator");
        System.out.println(g);
        int[] arr = { 1, 3, 5 };
        for (Integer i : arr) {
            System.out.printf("%d ", i);
        }
        System.out.println();
        int j = -3;
        OUT: while (true) {
            for(;;) {
                while (j < 10) {
                    if (j == 0) {
                        break OUT;
                    } else {
                        j++;
                        System.out.printf("%d ",j);
                    }
                }
            }
        }
        System.out.println("end");
    }

    static public int defineLevel(String role) {
        return switch (role) {     // или role.toLowerCase()
            case "guest" -> 1;
            case "client" -> 2;
            case "moderator" -> 3;
            case "admin" -> 4;
            default -> throw new IllegalArgumentException(); // или собственное исключение
        };
    }
}
