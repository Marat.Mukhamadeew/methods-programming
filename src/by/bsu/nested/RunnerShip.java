package by.bsu.nested;

public class RunnerShip {
    public static void main(String[] args) {
        // Вызов статического метода
        Ship.LifeBoat.down();
        // Создание объекта статического класса
        Ship.LifeBoat lifeBoat = new Ship.LifeBoat();
        // Вызов обычного метода
        lifeBoat.swim();
    }
}
