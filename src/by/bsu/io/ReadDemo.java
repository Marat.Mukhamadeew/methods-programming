package by.bsu.io;

import java.io.FileReader;
import java.io.IOException;

public class ReadDemo {
    public static void main(String[] args) {
        int b, count = 0;
//        FileInputStream is = null;  // Альтернатива
        try (FileReader is = new FileReader("data\\file.txt")) {
//            is = new FileInputStream(f);
            while ((b = is.read()) != -1) {   // Чтение
                System.out.println((char) b);
                count++;
            }
            System.out.print("\n число байт = " + count);
        } catch (IOException e) {
            System.err.println("Ошибка файла: " + e);
        }
    }
}
