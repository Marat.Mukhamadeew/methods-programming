package by.bsu.io;

import java.io.*;

public class DemoWriter {
    public static void main(String[] args) {
        File file = new File("res.txt");
        double[] v = { 1.10, 1.2, 1.401, 5.01, 6.017, 7, 8 };
        FileWriter fw = null;
        BufferedWriter bw = null;
        PrintWriter pw = null;
        try {
            fw = new FileWriter(file, true);
            bw = new BufferedWriter(fw);
            pw = new PrintWriter(bw);
            for (double d : v) {
                pw.printf("Java %.2g%n", d);    // Запись прямо в файл
            }
        } catch (IOException e) {
            System.err.println("Ошибка открытия потока: " + e);
        } finally {
            if (pw != null) {
                // Закрывать нужно только внешний поток
                pw.close();
            }
        }
    }
}
