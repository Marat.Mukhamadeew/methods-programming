package by.bsu.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class WriteRunner {
    public static void main(String[] args) {
        String pArray[] = {"2013", "Java SE 8"};
        File fByte = new File("data\\byte.data");
        File fSymb = new File("data\\symbol.txt");
        try (FileOutputStream fos = new FileOutputStream(fByte);
             FileWriter fw = new FileWriter(fSymb)) {
            for (String a : pArray) {
                fos.write(a.getBytes());
                fw.write(a);
            }
        } catch (IOException e) {
            System.err.println("Ошибка записи: " + e);
        }
    }
}
