package by.bsu.set;

import java.util.EnumSet;

public class UseEnumSet {
    public static void main(String[] args) {
        // Множество japanAuto содержат элементы типа enum из интервала, определенного двумя элементами
        EnumSet<CarManufacturer> japanAuto =
                EnumSet.range(CarManufacturer.TOYOTA, CarManufacturer.SUZUKI);
        // Множество other будет содержать все элементы, не содержащиеся в множестве japanAuto
        EnumSet<CarManufacturer> other = EnumSet.complementOf(japanAuto);
        System.out.println(japanAuto);
        System.out.println(other);
        action("audi", japanAuto);
        action("suzuki",japanAuto);
    }

    public static void action(String auto, EnumSet<CarManufacturer> set) {
        CarManufacturer cm = CarManufacturer.valueOf(auto.toUpperCase());
        if (set.contains(cm)) {
            // Обработка
            System.out.println("Обработан: " + cm);
        } else {
            System.out.println("Обработка невозможна: " + cm);
        }
    }
}
