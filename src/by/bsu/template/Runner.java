package by.bsu.template;

public class Runner {
    public static void main(String[] args) {
        // Параметризация типом Integer
        Message<Integer> ob1 = new Message<>();
        ob1.setValue(1);
        int v = ob1.getValue();
        System.out.println(v);
        // Параметризация типом String
        Message<String> ob2 = new Message<>("Java");
        String v2 = ob2.getValue();
        System.out.println(v2);
//        ob1 = ob2;
        Message ob3 = new Message<>();  // Сырой тип
        ob3 = ob1;
        System.out.println(ob3.getValue());
//        ob3.setValue(new Byte((byte) 1));
        ob3.setValue("Java SE 7");
        System.out.println(ob3);
        ob3.setValue(71);
        System.out.println(ob3);
        ob3.setValue(null);

    }
}
