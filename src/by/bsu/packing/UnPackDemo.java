package by.bsu.packing;

public class UnPackDemo {
    public static void main(String[] args) {
        // Расположение и имя архива
        String nameJar = "example.jar";
        // Куда файлы будут распакованы
        String destinationPath = "c:\\temp\\";
        new UnPackJar().unPack(destinationPath,nameJar);
    }
}
