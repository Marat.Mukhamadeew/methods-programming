package by.bsu.console;

import java.io.IOException;
import java.io.InputStreamReader;

public class ReadCharRunner {
    public static void main(String[] args) {
        InputStreamReader reader = new InputStreamReader(System.in);
        System.out.println("Encoding: " + reader.getEncoding());
        int x;
        try {
            x = System.in.read();
            char ch = (char) x;
            System.out.println("Код символа: " + ch + " = " + x);
        } catch (IOException e) {
            System.err.println("Ошибка ввода " + e);
        }
    }
}
