package by.bsu.synch;

import java.io.FileWriter;
import java.io.IOException;

public class Resource {
    private FileWriter fileWriter;
    public Resource(String file) throws IOException {
        // Проверка наличия файла
        fileWriter = new FileWriter(file, true);
    }

    public synchronized void writing(String str, int i) {
        try {
            fileWriter.append(str).append(String.valueOf(i));
            System.out.print(str + i);
            Thread.sleep((long) (Math.random() * 50));
            fileWriter.append("->").append(String.valueOf(i)).append(" ");
            System.out.println("->" + i + " ");
        } catch (IOException e) {
            System.err.println("Ошибка файла: " + e);
        } catch (InterruptedException e) {
            System.err.println("Ошибка потока: " + e);
        }
    }

    public void close() {
        try {
            fileWriter.close();
        } catch (IOException e) {
            System.err.println("Ошибка закрытия файла: " + e);
        }
    }
}
