package by.bsu.gc;

public class Manager {
    private int id;

    public Manager(int value) {
        id = value;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            // Код освобождения ресурсов
            System.out.println("Объект будет удален, id = " + id);
        } finally {
            super.finalize();
        }
    }
}
