package by.bsu.dumb;

public class Dumb{
    { this.id = 6; }
    int id;
    Dumb() {
        System.out.println("Конструктор класса Dumb ");
        // Вызов потенциального полиморфного метода - плохо
        id = this.getId();
        System.out.println(" id = " + id);
    }

    int getId() {
        System.out.println("getId() класса Dumb ");
        return id;
    }
}

class Dumber extends Dumb {
    int id = 9; //Получится два поля с одинаковыми именами
    Dumber() {
        System.out.println("Конструктор класса Dumber ");
        id = this.getId();
        System.out.println(" id = " + id);
    }

    @Override
    int getId() {
        System.out.println("getId класса Dumber ");
        return id;
    }
}

class Test {
    public static void main(String[] args) {
        Dumb d = new Dumber();
        Dumb d1 = new Dumb();
    }
}