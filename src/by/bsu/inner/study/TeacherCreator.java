package by.bsu.inner.study;

public class TeacherCreator {

    public static AbstractTeacher createTeacher(int id) {
        // Объявление класса внутри метода
        class Rector extends AbstractTeacher {
            public Rector(int id) {
                super(id);
            }
            @Override
            public boolean excludeStudent(String name) {
                return name != null;
            }
        } // Конец внутреннего класса
        if (isRector(id)) {
            return new Rector(id);
        } else return new Teacher(id);
    }
    private static boolean isRector(int id) {
        // Проверка id
        return id == 6; // stub
    }
}
