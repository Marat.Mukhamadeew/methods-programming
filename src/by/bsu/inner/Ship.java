package by.bsu.inner;

public class Ship {
    // Поля и конструкторы
//    private Engine eng;
    // abstract, final, private, protected - допустимы

    public class Engine { // Определение внутреннего (inner) класса
        // Поля и методы
        public void launch() {
            System.out.println("Запуск двигателя");
        }
    }   // Конец объявления внутреннего класса

    public final void init() {      // Метод внешнего класса
        Engine eng = new Engine();
        eng.launch();
    }
}

