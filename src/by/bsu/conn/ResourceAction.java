package by.bsu.conn;

public class ResourceAction {
    public void doAction() {
        SameResource sr = null;
        try {
            // Реализация - захват ресурсов
            sr = new SameResource(); // Возможна генерация исключения
            // Реализация - использование ресурсов
            sr.execute();   // Возможна генерация исключения
//            sr.close(); // Освобождение ресурсов (некорректно)
        } finally {
            // Освобождение ресурсов (корректно)
            if (sr != null) {
                sr.close();
            }
        }
        System.out.print("After finally");
    }
}
