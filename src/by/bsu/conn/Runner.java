package by.bsu.conn;

public class Runner {
    public static void main(String[] args) {
        SameResource f = new SameResource();    // SameResource f = null;
        try {   // Необязателен только при гарантированной корректности значения
            Connector.loadResource(f);
        } catch (IllegalArgumentException e) {
            System.err.println("Обработка unchecked - исключения вне метода: " + e);
        }
    }
}
