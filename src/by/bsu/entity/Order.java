package by.bsu.entity;

public class Order {
    private int id;     // Переменная экземпляра класса
    static int bonus;   // переменная класса
    public final int MIN_TAX = 8 + (int) (Math.random() * 5);   // константа экземпляра класса
    public final static int PURCHASE_TAX = 6;                   // константа класса

    public double calculatePrice(double price, int counter) {
        double amount = (price - bonus) * counter;
        double tax = amount * PURCHASE_TAX / 100;
        return amount + tax;
    }

    public static void main(String[] args) {
        Order order = new Order();
        double result = order.calculatePrice(55.8, 50);
        System.out.println(result);
        System.out.println(order.MIN_TAX);
    }
}
