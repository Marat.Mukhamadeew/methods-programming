package by.bsu.entity;

import java.util.Vector;

public class Student implements Cloneable {
    private int id = 71;
    private String name;
    private int age;
    private Vector<Byte> v = new Vector<>();    // Список оценок - изменяемое поле

    public Student() {
    }

    public Student(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Student other = (Student) obj;
        if (age != other.age) return false;
        if (id != other.id) return false;
        if (name == null) {
            return other.name == null;
        } else return name.equals(other.name);
    }

    @Override
    public int hashCode() {
        return (int) (31 * id + age + ((name == null) ? 0 : name.hashCode()));
    }

    @Override
    public String toString() {
        return getClass().getName() + "@name@" + name + " id: " + id + " age: " + age;
    }

//    @Override
//    public Object clone() throws CloneNotSupportedException { // Переопределение
//        return super.clone();   // Вызов базового метода
//    }

    public Student clone() throws CloneNotSupportedException{    // Метод-подставка
        Student copy = null;
        try {
            copy = (Student) super.clone();
            copy.v = (Vector<Byte>) v.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return copy;
    }
}
