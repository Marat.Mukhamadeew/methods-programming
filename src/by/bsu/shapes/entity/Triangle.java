package by.bsu.shapes.entity;
import static java.lang.Math.*;

public class Triangle extends AbstractShape {
    private double b;
    private double angle;      // Угол между сторонами в радианах

    public Triangle(double a, double b, double angle) {
        super(a);
        this.b = b;
        this.angle = angle;
    }

    public double getB() {
        return b;
    }

    public double getAngle() {
        return angle;
    }

    public double getC() {
        return (b * cos(angle)) * 2 + b * 2 * sin(2 * angle);
    }
}
