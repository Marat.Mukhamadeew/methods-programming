package by.bsu.shapes.action;

import by.bsu.shapes.entity.AbstractShape;

public interface ILineGroupAction {
    double computePerimeter(AbstractShape shape);
}
