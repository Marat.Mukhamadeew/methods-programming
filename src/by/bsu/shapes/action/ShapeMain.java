package by.bsu.shapes.action;

import by.bsu.shapes.entity.Rectangle;
import by.bsu.shapes.entity.Triangle;

public class ShapeMain {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(2, 3);
        IShapeAction<Rectangle> rectAction = new RectangleAction();
        Triangle triangle = new Triangle(3, 4, Math.PI / 6);
        IShapeAction<Triangle> triangleAction = new TriangleAction();
        System.out.println("Square rectangle: " + rectAction.computeSquare(rectangle));
        System.out.println("Perimeter rectangle: " + rectAction.computePerimeter(rectangle));
        System.out.println("Square triangle: " + triangleAction.computeSquare(triangle));
        System.out.println("Perimeter triangle: " + triangleAction.computePerimeter(triangle));
//        trAction.computePerimeter(rectShape); // ошибка компиляции
    }
}
