package by.bsu.shapes.action;

import by.bsu.shapes.entity.Rectangle;

public class RectangleAction implements IShapeAction<Rectangle> {
    @Override   // Реализация метода из интерфейса
    public double computeSquare(Rectangle shape) {  // Площадь прямоугольника
        return shape.getA() * shape.getB();
    }

    @Override
    public double computePerimeter(Rectangle shape) {
        return 2 * (shape.getA() + shape.getB());
    }
}
