package by.bsu.transfer.bean;

public class Account {
    private long id;
    private double amount;

    public Account() {
        super();        // Необязательно указывать
    }

    public Account(long id) {
        super();        // Необязательно указывать
        this.id = id;
    }

    public Account(long id, double amount) {
        super();        // Необязательно указывать
        this.id = id;
        this.amount = amount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void addAmount(double amount) {
        // Данный метод в общем случае можно объявить в другом классе
        this.amount += amount;
    }

    public static void main(String[] args) {
        Account account = new Account(5,88.8);
        System.out.println(account.getAmount());
        account.addAmount(55.5);
        System.out.println(account.getAmount());
    }
}
