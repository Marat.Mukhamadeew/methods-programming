package by.bsu.transfer;

import by.bsu.transfer.bean.Account;

public class TransferAction {
    public static double transactionAmount;
    private int id;

    public static boolean transferInfoAccount(Account from, Account to) {
        // Определение остатка
        double demand = from.getAmount() - transactionAmount;
        // Проверка остатка и перевод суммы
        if (demand >= 0) {
            from.setAmount(demand);
            to.addAmount(transactionAmount);
            return true;
        } else {
            return false;
        }
    }

    public void increaseAmount() {
        transactionAmount++;
    }
}
