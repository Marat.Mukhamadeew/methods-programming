package by.bsu.proj.accountlogic;

public interface AccountAction {
    // По умолчанию все методы public abstract
    boolean openAccount();
    boolean closeAccount();
    void blocking();
    void unBlocking();
    double depositInCash(int accountNumber, int amount);
    boolean withdraw(int accountNumber, int amount);
    boolean convert(double amount);
    boolean transfer(double amount);
}
