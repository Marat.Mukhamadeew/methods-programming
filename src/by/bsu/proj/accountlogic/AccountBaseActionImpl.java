package by.bsu.proj.accountlogic;

import by.bsu.proj.annotation.BankingAnnotation;
import by.bsu.proj.annotation.SecurityLevelEnum;

public class AccountBaseActionImpl implements AccountOperationManager {
   @BankingAnnotation(securityLevel = SecurityLevelEnum.HIGH)
    public double depositInCash(int accountNumber, int amount) {
       // Зачисление на депозит
       return 0;    // stub
   }

   @BankingAnnotation(securityLevel = SecurityLevelEnum.HIGH)
    public boolean withdraw(int accountNumber, int amount) {
       // Снятие суммы, если не превышает остаток
       return true;     // stub
   }

   @BankingAnnotation(securityLevel = SecurityLevelEnum.LOW)
    public boolean convert(double amount) {
       // Конвертировать сумму
       return true;     // stub
   }

    @BankingAnnotation
    public boolean transfer(int accountNumber, double amount) {
       // Перевести сумму на счет
       return true;     // stub
   }
}
