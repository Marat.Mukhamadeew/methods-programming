package by.bsu.polymorph;

import java.text.ParseException;

public class StoneAction {
    public void buildHouse(Stone stone) {
        try {
            stone.build("some info");
            //  Предусмотрена обработка ParseException и его подклассов
        } catch (ParseException e) {
            System.err.print(e);
        }
    }
}
