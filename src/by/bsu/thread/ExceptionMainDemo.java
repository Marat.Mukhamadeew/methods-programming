package by.bsu.thread;

public class ExceptionMainDemo {
    public static void main(String[] args) {
        new SimpleThread().start();
        System.out.println("End of main with exception");
        throw new RuntimeException();
    }
}
