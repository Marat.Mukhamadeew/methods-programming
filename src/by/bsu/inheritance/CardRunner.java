package by.bsu.inheritance;

public class CardRunner {
    public static void main(String[] args) {
        AbstractCardAction action;  // Можно объявлять ссылку
//        action = new AbstractCardAction();  // Объект создать нельзя
        action = new CreditCardAction();
        action.doPayment(100);
        System.out.println(action.checkLimit());
    }
}
