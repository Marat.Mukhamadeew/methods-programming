package by.bsu.inheritance;

public class CreditCardAction extends AbstractCardAction {
    // Поля, конструкторы и методы

    @Override
    public void doPayment(double amountPayment) {
        System.out.println("Complete from credit card");
    }
}
