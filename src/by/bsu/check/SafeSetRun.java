package by.bsu.check;

import by.bsu.collection.Order;

import java.util.Collections;
import java.util.HashSet;

public class SafeSetRun {
    public static void main(String[] args) {
        HashSet orders;
//        orders = new HashSet(); // Заменяемы код на jdk 1.4 и ниже
        orders = (HashSet) Collections.checkedSet(new HashSet<>(), Order.class);
        orders.add(new Order(22,14f));
//        orders.add(new Item(212, 999f,"Java"));
    }
}
