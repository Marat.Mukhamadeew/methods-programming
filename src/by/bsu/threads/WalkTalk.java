package by.bsu.threads;

public class WalkTalk {
    public static void main(String[] args) {
        // Новые объекты потоков
        TalkThread talk = new TalkThread();
        Thread walk = new Thread(new WalkRunnable());
        // Запуск потоков
        talk.start();
        walk.start();
//        WalkRunnable w = new WalkRunnable();    // Просто объект, не поток
//        w.run();    или talk.run();             // Выполняется метод, но поток не запускается
    }
}
