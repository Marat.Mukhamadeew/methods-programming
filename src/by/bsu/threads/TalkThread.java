package by.bsu.threads;

public class TalkThread extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println("Talking");
            try {
                Thread.sleep(700);    // Остановка на 7 миллисекунд
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
    }
}
