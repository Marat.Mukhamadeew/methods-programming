package by.bsu.scalability;

public class TestRunner {
    public static void main(String[] args) {
        TestAction bt = new TestAction();
        AbstractQuest[] test = bt.generateTest(60, 2);      // 60 вопросов 2-х видов
        // Здесь должен быть код процесса прохождения теста ...
        bt.checkTest(test);     // Проверка теста
    }
}
