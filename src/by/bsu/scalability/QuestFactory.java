package by.bsu.scalability;

public class QuestFactory {     // Шаблон Factory method (упрощенный)
    public static AbstractQuest getQuestFromFactory(int mode) {
        return switch (mode) {
            case 0 -> new DragonDropQuest();
            case 1 -> new SingleChoiseQuest();
            default -> throw new IllegalArgumentException("illegal mode");

//             assert false;    // плохо
//             return true;     // еще хуже
        };
    }
}
