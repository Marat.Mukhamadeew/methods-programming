package by.bsu.scalability;

import java.util.Random;

public class TestAction {
    public AbstractQuest[] generateTest(final int NUMBER_QUESTS, int maxMode) {
        AbstractQuest[] test = new AbstractQuest[NUMBER_QUESTS];
        for (int i = 0; i < test.length; i++) {
            int mode = new Random().nextInt(maxMode);   // stub
            // Заполнение массива объектами-вопросами
            test[i] = QuestFactory.getQuestFromFactory(mode);
        }
        return test;
    }

    public int checkTest(AbstractQuest[] test) {
        int counter = 0;
        for (AbstractQuest s : test) {
            // Вызов полиморфного метода
            if (s.check(new Answer())) {
                ++counter;
            }
        }
        return counter;
    }
}
