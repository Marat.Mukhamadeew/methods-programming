package by.bsu.collection;

import java.util.ArrayList;
import java.util.List;

public class RunIterator {
    public static void main(String[] args) {
        ArrayList<Order> orders = new ArrayList<>() {
            {
                add(new Order(231));
                add(new Order(389));
                add(new Order(217));
            }
        };
        FindOrder fo = new FindOrder();
        List<Order> res = fo.findBiggerAmountOrder(10f,orders);
        System.out.println(res);
    }
}
