package by.bsu.collection;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class OrderQueueAction {
    public static void main(String[] args) {
        LinkedList<Order> orders = new LinkedList<>() {
            {
                add(new Order(231,12.f));
                add(new Order(389,2.9f));
                add(new Order(217,1.7f));
            }
        };
        Queue<Order> queueA = orders; // Создание очереди
        queueA.offer(new Order(222,9.7f));  // Элемент добавится
        orderProcessing(queueA);    // Обработка очереди
        if (queueA.isEmpty()) {
            System.out.println("Queue of Orders is empty");
        }
        PriorityQueue<Order> orders1 = new PriorityQueue<>();
        orders1.add(new Order(546,53.f));
        orders1.add(new Order(146,13.f));
    }

    public static void orderProcessing(Queue<Order> queue) {    // Заменить void -> boolean
        Order ob = null;
        // Заменить while -> do{} while
        while ((ob = queue.poll()) != null) {   // Извлечение с удалением
            System.out.println("Order #" + ob.getOrderId() + " is processing");
            // verifying and processing
        }
    }
}
