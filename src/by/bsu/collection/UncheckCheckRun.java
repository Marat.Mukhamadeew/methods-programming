package by.bsu.collection;

import java.util.ArrayList;

public class UncheckCheckRun {
    public static void main(String[] args) {
        ArrayList raw = new ArrayList() {   // сырая коллекция - raw type
            {   // Логический блок анонимного класса
                add(new Order(231));
                add(new Item(23154,120.f,"Xerox"));
                add(new Order(217));
            }
        };
        // При извлечении требуется приведение типов
        Order or1 = (Order) raw.get(0);
        Item or2 = (Item) raw.get(1);
        Order or3 = (Order) raw.get(2);

        for (Object obj : raw) {
            System.out.println("raw " + obj);
        }

        ArrayList<Order> orders = new ArrayList<>() {
            {
                add(new Order(231));
                add(new Order(389));
                add(new Order(217));
//                add(new Item(23145,120.f,"Xerox"));
            }
        };
        for (Order or : orders) {
            System.out.println("Order: " + or);
        }
    }
}
