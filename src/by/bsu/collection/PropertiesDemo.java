package by.bsu.collection;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertiesDemo {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
        // загрузка пар ключ-значение через поток ввода-вывода
            properties.load(new FileReader("prop\\database.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String driver = properties.getProperty("db.driver");
        // следующих двух ключей в файле нет
        String maxIdle = properties.getProperty("db.maxIdle"); // будет присвоен null
        // значение "20" будет присвоено ключу, если он не будет найден в файле
        String maxActive = properties.getProperty("db.maxActive", "20");
        System.out.println(driver);
        System.out.println(maxIdle);
        System.out.println(maxActive);
    }
}
