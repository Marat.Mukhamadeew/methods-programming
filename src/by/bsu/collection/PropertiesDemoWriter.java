package by.bsu.collection;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

public class PropertiesDemoWriter {
    public static void main(String[] args) {
        Properties properties = new Properties();
        try {
            // Установка значений экземпляру
            properties.setProperty("db.driver", "com.mysql.jdbc.Driver");
            // props.setProperty("db.url", "jdbc:mysql://127.0.0.1:3306/testphones");
            properties.setProperty("db.user", "root");
            properties.setProperty("db.password", "pass");
            properties.setProperty("db.poolsize", "5");
            // запись properties-файла в папку prop проекта
            properties.store(new FileWriter("prop" + File.separator + "database.properties"), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
