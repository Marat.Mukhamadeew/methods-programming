package by.bsu.collection;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Order implements Iterable<Item>, Comparable<Order> {
    private int orderId;
    private List<Item> listItems;
    private float amount;   // Не нужен, т.к. сумму можно вычислить
    // Поля и методы описания подробностей заказа


    public Order(int orderId) {
        this.orderId = orderId;
    }

    public Order(int orderId, float amount) {
        this.orderId = orderId;
        this.amount = amount;
    }

    public Order(int orderId, List<Item> listItems) {
        this.orderId = orderId;
        this.listItems = listItems;
    }

    public int getOrderId() {
        return orderId;
    }

    public List<Item> getListItems() {
        return Collections.unmodifiableList(listItems);
    }
    // Некоторые делегированные методы интерфейсов List и Collection

    public boolean add(Item e) {
        return listItems.add(e);
    }

    public Item get(int index) {
        return listItems.get(index);
    }

    public Item remove(int index) {
        return listItems.remove(index);
    }

    @Override
    public Iterator<Item> iterator() {
        return listItems.iterator();
    }

    @Override
    public int compareTo(Order o) {
        return this.orderId - o.orderId;
    }

    @Override
    public String toString() {
        return "Order [orderId = " + orderId + "]";
    }
}
