package by.bsu.construction;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

public class ConcreteResource extends Resource {
    // Ранее созданный конструктор
    public ConcreteResource(String name) throws FileNotFoundException {
        super(name);
        // more code
    }

    // Ранее созданный конструктор
    public ConcreteResource() throws IOException {
        super("file.txt");
        // more code
    }

//    // Новый конструктор
//    public ConcreteResource(String name, int mode) {
//        super(name);
//        // more code
//    }
//
//    // Ошибка компиляции
//    public ConcreteResource(String name, int mode, String type) throws ParseException {
//        super(name);
//        // more code
//    }
}
