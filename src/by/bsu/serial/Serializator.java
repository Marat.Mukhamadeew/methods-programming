package by.bsu.serial;

import java.io.*;

public class Serializator {
    public boolean serialization(Student s, String fileName) {
        boolean flag = false;
        File file = new File(fileName);
        ObjectOutputStream ostream = null;
        try {
            FileOutputStream fos = new FileOutputStream(file);
            if (fos != null) {
                ostream = new ObjectOutputStream(fos);
                ostream.writeObject(s); // Сериализация
                flag = true;
            }
        } catch (FileNotFoundException e) {
            System.err.println("Файл не может быть создан: " + e);
        } catch (NotSerializableException e) {
            System.err.println("Класс не поддерживает сериализацию: " + e);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (ostream != null) {
                    ostream.close();
                }
            } catch (IOException e) {
                System.err.println("Ошибка закрытия потока: " + e);
            }
        }
        return flag;
    }

    public Student deserialization(String fileName) throws InvalidObjectException {
        File file = new File(fileName);
        ObjectInputStream istream = null;
        try {
            FileInputStream fis = new FileInputStream(file);
            istream = new ObjectInputStream(fis);
            // Десериализация
            return (Student) istream.readObject();
        } catch (ClassNotFoundException e) {
            System.err.println("Класс не существует: " + e);
        } catch (FileNotFoundException e) {
            System.err.println("Файл для десереализации не существует: " + e);
        } catch (InvalidClassException e) {
            System.err.println("Несовпадение версий классов: " + e);
        } catch (IOException e) {
            System.err.println("Общая I/O ошибка: " + e);
        } finally {
            try {
                if (istream != null) {
                    istream.close();
                }
            } catch (IOException e) {
                System.err.println("Ошибка закрытия потока ");
            }
        }
        throw new InvalidObjectException("Объект не восстановлен");
    }
}
