package by.bsu.serial;

import java.io.InvalidObjectException;

public class RunnerSerialization {
    public static void main(String[] args) {
        // Создание и запись объекта
        Student student = new Student("MMF", "Goncharenko", 1, "G017s9");
        System.out.println(student);
        String file = "demo.data";
        Serializator sz = new Serializator();
        boolean b = sz.serialization(student, file);
        Student.faculty = "GEO";    // Изменение значения static-поля
        // Чтение и вывод объекта
        Student res = null;
        try {
            res = sz.deserialization(file);
        } catch (InvalidObjectException e) {
            // Обработка
            e.printStackTrace();
        }
        System.out.println(res);
    }
}
