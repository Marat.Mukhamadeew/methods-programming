package by.bsu.strings;

public class DemoStringBuffer {
    public static void main(String[] args) {
        StringBuffer sb = new StringBuffer();
        System.out.println("Длина -> " + sb.length());
        System.out.println("Размер -> " + sb.capacity());
//        sb = "Java";    //  Ошибка, только для класса String
        sb.append("Java");
        System.out.println("Строка -> " + sb);
        System.out.println("Длина -> " + sb.length());
        System.out.println("Размер -> " + sb.capacity());

        System.out.println(sb.append(" Internalization"));
        System.out.println("Строка -> " + sb);
        System.out.println("Длина -> " + sb.length());
        System.out.println("Размер -> " + sb.capacity());

        System.out.println("Реверс -> " + sb.reverse());
    }
}
