package by.bsu.sample;

class Base {
    public static void go() {
        System.out.println("Метод из Base");
    }
}

class Sub extends Base {
    public static void go() {
        System.out.println("Метод из Sub");
    }
}

public class Runner {
    public static void main(String[] args) {
        Base ob = new Sub();
//        Sub ob2 = new Base();     // Объект супер класса не может иметь ссылку подкласса
        // Нестатический вызов статического метода
        ob.go();    // Предупреждение компилятора о некорректном вызове
        Base.go();
        Sub.go();
    }
}
