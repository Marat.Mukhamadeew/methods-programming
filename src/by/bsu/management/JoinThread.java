package by.bsu.management;

public class JoinThread extends Thread {
    public JoinThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        long timeout = 0;
        System.out.println("Старт потока " + getName());
        try {
            switch (getName()) {
                case "First" -> timeout = 5_000;
                case "Second" -> timeout = 1_000;
            }
            Thread.sleep(timeout);
            System.out.println("Завершение потока " + getName());
        } catch (InterruptedException e) {
            System.err.println(e);
        }
    }
}
