package by.bsu.logic;

public class Department {
    {
        System.out.println("logic (1) id = " + this.id);
        // Проверка и инициализация параметров конкретного объекта
    }

    static {
        System.out.println("static logic");
        // Проверка и инициализация базовых параметров, необходимых
        // для фукнционирования приложения (класса)
    }

    private int id = 7;

    public Department(int id) {
        this.id = id;
        System.out.println("Конструктор id = " + id);
    }

    public int getId() {
        return id;
    }

    {
        // не очень хорошее расположение логического блока
        System.out.println("logic (2) id = " + id);
    }
}
