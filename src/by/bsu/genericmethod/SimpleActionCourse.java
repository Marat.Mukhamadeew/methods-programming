package by.bsu.genericmethod;

public class SimpleActionCourse {
    public <T1 extends Course> SimpleActionCourse(T1 course) {
        // Реализация
    }
    public <T2> SimpleActionCourse() {
        // Реализация
    }
    public <T3 extends Course> float calculateMark(T3 course) {
        // Реализация
        return 0;
    }
    public <T4> boolean printReport(T4 course) {
        // Реализация
        return true;
    }
    public <T5> void check() {
        // Реализация
    }
}
class Course{}
