package by.bsu.enums;

import java.util.Arrays;

public class MelomanRunner {
    public static void main(String[] args) {
        MusicStyle ms = MusicStyle.BLUES;     // Инициализация
        System.out.print(ms);
        switch (ms) {
            case JAZZ -> System.out.println(" is Jazz");
            case CLASSIC -> System.out.println(" is Classic");
            case ROCK -> System.out.println(" is Rock");
            default -> System.out.println(" Unknown music style: " + ms);
        }
        System.out.println(Arrays.toString(MusicStyle.values()));
        System.out.println(MusicStyle.valueOf("ROCK"));
        System.out.println(MusicStyle.JAZZ.ordinal());
        System.out.println(MusicStyle.BLUES.name());
    }
}
