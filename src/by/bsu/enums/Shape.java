package by.bsu.enums;

public enum Shape {
    RECTANGLE, SQUARE, CIRCLE, TRIANGLE {   // Анонимный класс

        public double computeSquare() {     // Версия для TRIANGLE
            return this.getA() * this.getB() / 2;
        }
    };
    private double a;
    private double b;

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public void setShape(double a, double b) {
        if ((a <= 0 || b <= 0) || a != b && this == SQUARE) {
            throw new IllegalArgumentException();
        }
        this.a = a;
        this.b = b;
    }

    public double computeSquare() {     // Версия для RECTANGLE и SQUARE
        return a * b;
    }

    @Override
    public String toString() {
        return name() + "-> a = " + a + ", b = " + b;
    }

    // Метод класса перечисления
    public double defineSquare(double... x) {
        // Проверка параметров
        return switch (this) {
            case RECTANGLE -> x[0] * x[1];
            case TRIANGLE -> x[0] * x[1] / 2;
            case CIRCLE -> Math.pow(x[0], 2) * Math.PI;
            case SQUARE -> x[0] * x[1] / 3;
        };
    }
}
